# coding=utf-8
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from SelfExceptionError import *
from selenium.webdriver.common.action_chains import ActionChains
# from selenium.webdriver.common.touch_actions import TouchActions


# from DebugBrowser import * 
import time
import random
import datetime
import re
import configparser
import random

def getRandomNumberByRange(start, end):
    return random.randint(start,end)/10 +1


def getGoodINIUrl():
    cfg1 = "youzhan.ini"
    conf = configparser.ConfigParser()
    conf.read(cfg1, encoding="utf-8-sig")
    #读取ini文件中的内容
    return conf.get("goods", "goodsurl")


# def getGoodINIPayUrl():
#     cfg1 = "youzhan.ini"
#     conf = configparser.ConfigParser()
#     conf.read(cfg1, encoding="utf-8-sig")
#     #读取ini文件中的内容
#     url = conf.get("goods", "payurl")
#     url = url.replace("\n",'')
#     if (len(url)>5):
#         return url


# def saveGoodINIPayUrl(url):
#     if (False == url):
#         url = '';

#     cfg1 = "youzhan.ini"
#     conf = configparser.ConfigParser()
#     conf.read(cfg1, encoding="utf-8-sig")
#     #读取ini文件中的内容
#     conf.set("goods", "payurl", url)
#     conf.write(open(cfg1, "w+"))


def getGoodINIPrice():
    cfg1 = "youzhan.ini"
    conf = configparser.ConfigParser()
    conf.read(cfg1, encoding="utf-8-sig")
    #读取ini文件中的内容
    return conf.get("goods", "price")



# def getspurl(): #获取shopurl.txt中的第一行字符串 获取youzhan地址
#     p = os.path.realpath('.')
#     with open( p+'\\shopurl.txt', 'r') as f:
#         lines = f.readlines()
#         return lines[1]


# def getPrice(): #Price.txt中的第一行字符串 获取价格
#     p = os.path.realpath('.')
#     with open( p+'\\shopurl.txt', 'r') as f:
#         lines = f.readlines()
#         return lines[0]


def currentTime(): #获取当前时间
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+">>>"

def prt(s): #打印日志
    txt = currentTime() + '>>>' + str(s)
    print(txt)
    savelog(txt)

def savelog(s): #打印报错日志
    txt = '\r\n' + currentTime() + '\r\n' + str(s)
    with open('./logs.txt', 'a', encoding='utf8') as f:
        f.write(txt)

def successlog(s): #打印下单成功的日志
    txt = '\r\n' + currentTime() + '::\r\n' + str(s)
    with open('./success.txt', 'a', encoding='utf8') as f:
        f.write(txt)
#------------------------匹配点击开始--------------------#
def ifid(driver, actname):  # 匹配id
    try:
        driver.find_element_by_id(actname)
        return True
    except:
        return False
def ifclass(driver, actname):  # 匹配id
    try:
        driver.find_element_by_class_name(actname)
        return True
    except:
        return False

def tapid(driver, actname):  # 匹配id
    try:
        driver.find_element_by_id(actname).click()
        return True
    except:
        return False

def tapclass(driver, actname):  # 匹配class
    try:
        driver.find_element_by_class_name(actname).click()
        return True
    except:
        #print('没有找到点击的class:'+str(actname)+'...正在重新查找中...')
        return False    

def valhtml(driver, actname):  # 匹配标签值
    try:
        driver.find_element_by_tag_name(actname).text
        return driver.find_element_by_tag_name(actname).text
    except:
        return False


def valclass(driver, actname):  # 匹配class值
    try:
        driver.find_element_by_class_name(actname).text
        return driver.find_element_by_valclassclass_name(actname).text
    except:
        return False

def wait_for_elem_xpath_func(browser, delay = 3,vByType='xpath', by_value = ""):    
    try:
        if (vByType=='xpath'):
            myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.XPATH , by_value)))
        if (vByType=='class_name'):
            myElem = WebDriverWait(browser, delay).until(EC.presence_of_element_located((By.CLASS_NAME , by_value))) 
    except Exception:
        print ("xpath: Loading took too much time!")
        return None
    return myElem

# def tapxpath(driver, actname):  # 表达式定位匹配 下方有示列
#     try:
#         driver.find_element_by_xpath(actname).click()
#         return True
#     except:
#         tapxpath(driver, actname)
#         #print('没有找到标签:'+str(actname)+'...正在重新查找中...')
#         return False

# def taphref(driver, actname):  # 匹配a标签链接中的值
#     try:
#         driver.find_element_by_link_text(actname).click()
#         manpp = 0
#         return True
#     except:
#         taphref(driver, actname)
#         print('没有找到a标签:'+str(actname)+'...正在重新查找中...')
#         return False








# def tapname(driver, actname):  # 匹配name
#     try:
#         driver.find_element_by_name(actname).click()
#         return True
#     except:
#         tapname(driver, actname)
#         print('没有找到:'+str(actname)+'...正在重新查找中...')
#         return False






# 示列
# dr.find_element_by_xpath("//*[@id='kw']")
# dr.find_element_by_xpath("//*[@name='wd']")
# dr.find_element_by_xpath("//input[@class='s_ipt']")
# dr.find_element_by_xpath("/html/body/form/span/input")
# dr.find_element_by_xpath("//span[@class='soutu-btn']/input")
# dr.find_element_by_xpath("//form[@id='form']/span/input")
# dr.find_element_by_xpath("//input[@id='kw' and @name='wd']")
# find_element_by_xpath("//*[@id='kw']")
#------------------------匹配点击end--------------------#

def bhzfc(zimu, zi):  # 匹配字符串中是否有指定的字符
    result = zi in zimu
    return result

def to_int(str):
    try:
        int(str)
        return int(str)
    except ValueError:  # 报类型错误，说明不是整型的
        try:
            float(str)  # 用这个来验证，是不是浮点字符串
            return int(float(str))
        except ValueError:  # 如果报错，说明即不是浮点，也不是int字符串。   是一个真正的字符串
            return False
