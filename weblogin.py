# coding=utf-8
# 第一版 2022/3/21 author: ylong52@21cn.com
# 网站登录，并备份登录的缓存

from http import server
from lib2to3.pgen2 import driver
from urllib.parse import urlparse
from myselenium import *
import sys
from LocalStorage import *
from browsermobproxy import Server
import json

class weblogin:
    
    #主程序
    def main(self):      
         
        # https://www.cnblogs.com/51testing/p/13870503.html  进行浏览器中途调试的方法类实现
        chrome_options2 = webdriver.ChromeOptions()
        
         
        # if ("1" != self.config.get("setup", "viewgoogle")):
        #     chrome_options2.add_argument('--headless') #无头浏览器

        chrome_options2.add_argument('--disable-gpu')
        chrome_options2.add_argument('--ignore-certificate-errors')
        chrome_options2.add_argument('--ignore-ssl-errors')

        # prefs = {"profile.managed_default_content_settings.images": 2}   #验证码拖图看不到
        # chrome_options2.add_experimental_option("prefs", prefs)

        # driver = webdriver.Chrome()
        # driver.set_window_size(500,800)
        # driver.get('https://artmeta.cn/')

        # chrome_options2 = DebugBrowser().debug_chrome();  #只启动一个google调试
      
        # options2.add_experimental_option('debuggerAddress',DebugBrowser().debug_chrome());
        # options2.add_argument("--auto-open-devtools-for-tabs");
        driver= webdriver.Chrome(options=chrome_options2)
        driver.set_window_size(600,800)
        #driver.get('https://artmeta.cn/pages/usercenter/usercenter')                       
        self.driver = driver
        self.doLogin()
        del self.driver
        del self.loginInfo
        return True
         
       
        

    def doLogin(self):
        _login_url = 'https://artmeta.cn/pages/login/index'
        self.driver.get(_login_url) #登录
        # time.sleep(0.5)
        # storage = LocalStorage(self.driver) 
        # _is_truename = storage.has("truename")
        # if (_is_truename):
        #     print("登录信息有效，无需重新登录")
        #     os._exit(0)
        # if (False == _is_truename):
        #     self.driver.get("https://artmeta.cn/pages/login/index") #登录

        trWhile = True
        while trWhile:
            time.sleep(1)
            _ret = self.watchStorage()
            if (_ret==True):
                trWhile=False
                
        
        print('保存登录信息：')
        print(self.loginInfo)

        conf = configparser.ConfigParser()    
        _section = "login"
        conf.remove_section(_section) 
        conf.add_section(_section)        
        # 往select添加key和value
        conf.set(_section, "truename", self.loginInfo['truename'])
        conf.set(_section, "UserLoginToken", self.loginInfo['UserLoginToken'])
        conf.set(_section, "userinfo", self.loginInfo['userinfo'])
        conf.set(_section, "UserId", self.loginInfo['UserId'])
        conf.set(_section, "操作时间", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) )

        cfg1 = self.loginInfo['truename']+".ini"
        
        newinifile = './logincookies/'+ cfg1;          
        conf.write(open(newinifile, "w",encoding="utf-8"))
        print('登录信息成功保存在：'+newinifile+"\n\n")
        return True
        


    def watchStorage(self):
        storage = LocalStorage(self.driver)      
        _is_truename = storage.has("truename")
        if (False == _is_truename):
            return False
        else:
            _login = {}; 
            _login['truename']= storage.get("truename")
            _login['UserLoginToken'] = storage.get("UserLoginToken") 
            _login['userinfo'] = storage.get("userinfo")   
            _login['UserId'] = storage.get("UserId")             
            self.loginInfo = _login 
            return True;
 
        

 


